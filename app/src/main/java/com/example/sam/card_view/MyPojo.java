package com.example.sam.card_view;

public class MyPojo
{

    int image;

    public MyPojo()
    {

    }

    public MyPojo(int image) {
        this.image = image;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image)
    {
        this.image = image;
    }
}
