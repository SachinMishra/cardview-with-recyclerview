package com.example.sam.card_view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {


    //myadapter class

    private ArrayList<MyPojo> arrayList;
    private Context context;
    private LayoutInflater layoutInflater;

    public MyAdapter()
    {


    }

    public MyAdapter(ArrayList<MyPojo> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }



    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view=layoutInflater.inflate(R.layout.myview,null,false);
        MyAdapter.MyViewHolder myViewHolder=new MyAdapter.MyViewHolder(view);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        myViewHolder.circleView.setImageResource(arrayList.get(i).getImage());


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }



    class MyViewHolder extends RecyclerView.ViewHolder{

        CircleImageView circleView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            circleView=itemView.findViewById(R.id.circleView);
        }
    }



}
